import time,hmac,hashlib,requests,json
from urllib.parse import urlencode

#v1.0
#Esta API sirve para consumir los servicios de https://www.yobit.net

class yobit(object):
	def __init__(self):
		self.key = "11ACE83BEDA23F67AB2C8B67C33310D5"
		self.secret = "a069f1123c6d15acd7777c1a65939a86"
		self.public = ['info', 'ticker', 'depth', 'trades', 'fee']
		self.private = ['getInfo', 'ActiveOrders','OrderInfo','CancelOrder','GetDepositAddress','Trade','WithdrawCoinsToAddress']

	def queryV3(self, method, values={}):

		if str(method) in self.public:
			print("public", method,": ")

			url = 'https://yobit.net/api/3/'+method

			for name, value in values.items():
			    url += '/'+value

			print(url)

			try:
				req = requests.get(url)

				return json.loads(req.text)

			except urllib.HTTPError as e:
				return e.read()


		elif str(method) in self.private:
			print("private", method,": ")

			url = 'https://yobit.net/tapi'

			payload = {'method':method, 'nonce':str(int(time.time()))}

			#add parameters
			for name, value in values.items():
				payload.update({name : value})   	

			body = urlencode(payload)

			secretHash = bytes(self.secret , 'latin-1')
			body = bytes(body, 'latin-1')
			
			sign = hmac.new(secretHash, body, hashlib.sha512).hexdigest()

			headers = {
			    'Content-Type': 'application/x-www-form-urlencoded',
			    'Key': self.key,
			    'Sign': sign
			}

			try:
				req = requests.post(url,data=payload,headers=headers)

				return json.loads(req.text)

			except urllib.HTTPError as e:
				return e.read()	

			


	def queryV2(self, method, values={}):
		if str(method) in self.public:
			print("public", method,": ")

			url = 'https://yobit.net/api/2/pair/'+method

			for name, value in values.items():
			    url = url.replace('pair', value, 3)

			try:    
				req = requests.get(url)

				return json.loads(req.text)

			except urllib.HTTPError as e:
				return e.read()		

	def getPublicInfo(self):
		#info
		values = {}
		serviceResp = self.queryV3('info', values)

		return serviceResp			

	def getPublicFee(self, pair):
		#fee
		serviceResp = yob.queryV2('fee', pair)

		return serviceResp

	def getPublicTicker(self, pair):
		#ticker
		serviceResp = yob.queryV3('ticker', pair)

		return serviceResp

	def getPublicDepth(self, pair):
		#depth
		serviceResp = yob.queryV3('depth', pair)

		return serviceResp

	def getPublicTrades(self, pair):
		#values = {"pair": "xem_usd"}
		serviceResp = yob.queryV3('trades', pair)	

		return serviceResp

	def privateInfo(self, data):
		#getinfo
		serviceResp = yob.queryV3('getInfo', data)

		return serviceResp

	def privateTrade(self, data):
		#getinfo
		serviceResp = yob.queryV3('Trade', data)

		return serviceResp

	def privateActiveOrders(self, data):
		#Trade
		serviceResp = yob.queryV3('ActiveOrders', data)

		return serviceResp
	
	def privateOrderInfo(self, data):
		#order info
		serviceResp = yob.queryV3('OrderInfo', data)

		return serviceResp

	def privateCanceledOrders(self, data):
		#Canceled orders	
		serviceResp = yob.queryV3('CancelOrder', data)

		return serviceResp

	def privateDepositAddress(self, data):
		#DepositAddress
		serviceResp = yob.queryV3('GetDepositAddress', data)
		return serviceResp

	def privateWithdrawCoinsToAddress(self, data):
		#Withdraw Coins Address
		serviceResp = yob.queryV3('WithdrawCoinsToAddress', data)
		return serviceResp

yob = yobit()
#------------------API publica-------------------------------

#info
#print(yob.getPublicInfo())

#fee
#data = {"pair": "xem_usd"}
#print(yob.getPublicFee(data))

#ticker informacion de una moneda
#data = {"pair": "xem_usd"}
#print(yob.getPublicTicker(data))

#depth
#data = {"pair": "xem_usd"}
#print(yob.getPublicDepth(data))

#trades
#data = {"pair": "xem_usd"}
#print(yob.getPublicTrades(data))


#---------------------API privada-------------------------

#info
#data = {}
#print(yob.privateInfo(data))

#Trade
#data = {"pair": "nyc_usd", "type": "buy", "rate": "112", "amount": "100"}
#print(yob.privateTrade(data))

#ActiveOrders
#data = {"pair": "xem_usd"}
#print(yob.privateActiveOrders(data))

#OrderInfo
#data = {"order_id": "206167381996163"}
#print(yob.privateOrderInfo(data))

#CancelOrder
#data = {"order_id": "206167381111163"}
#print(yob.privateCanceledOrders(data))

#deposite adress
#data = {"coinName": "XEM", "need_new": 0}
#print(yob.privateDepositAddress(data))

#Withdraw Coins to Address
#data = {"coinName": "NYC", "amount": '100000', 'address': 'RQRT7MyVFM3PxAXnt5FkBCiruPyaQLzcGM'}
#print(yob.privateWithdrawCoinsToAddress(data))