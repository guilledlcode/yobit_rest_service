
import time,hmac,hashlib,requests,json
from urllib.parse import urlencode

#v1.0
#Esta API sirve para consumir los servicios de https://www.yobit.net

class yobit(object):
	def __init__(self, key, secret):
		self.key = key
		self.secret = secret
		self.public = ['info', 'ticker', 'depth', 'trades', 'fee']
		self.private = ['getInfo', 'ActiveOrders','OrderInfo','CancelOrder','GetDepositAddress','Trade','WithdrawCoinsToAddress']

	def queryV3(self, method, values={}):

		if str(method) in self.public:
			print("public", method,": ")

			url = 'https://yobit.net/api/3/'+method

			for name, value in values.items():
			    url += '/'+value

			print(url)

			try:
				req = requests.get(url)

				return json.loads(req.text)

			except urllib.HTTPError as e:
				return e.read()


		elif str(method) in self.private:
			print("private", method,": ")

			url = 'https://yobit.net/tapi'

			payload = {'method':method, 'nonce':str(int(time.time()))}

			#add parameters
			for name, value in values.items():
				payload.update({name : value})   	

			body = urlencode(payload)

			secretHash = bytes(self.secret , 'latin-1')
			body = bytes(body, 'latin-1')
			
			sign = hmac.new(secretHash, body, hashlib.sha512).hexdigest()

			headers = {
			    'Content-Type': 'application/x-www-form-urlencoded',
			    'Key': self.key,
			    'Sign': sign
			}

			try:
				req = requests.post(url,data=payload,headers=headers)

				return json.loads(req.text)

			except urllib.HTTPError as e:
				return e.read()	

			


	def queryV2(self, method, values={}):
		if str(method) in self.public:
			print("public", method,": ")

			url = 'https://yobit.net/api/2/pair/'+method

			for name, value in values.items():
			    url = url.replace('pair', value, 3)

			try:    
				req = requests.get(url)

				return json.loads(req.text)

			except urllib.HTTPError as e:
				return e.read()		

key = ""
secret = ""

yob = yobit(key, secret)

#-------------------------------------------------------------------
#API publica

#info
#values = {}
#serviceResp = yob.queryV3('info', values)

#fee
values = {"pair": "nyc_usd"}
serviceResp = yob.queryV2('fee', values)

#ticker esta caido
#values = {"pair": "nyc_usd"}
#serviceResp = yob.queryV3('ticker', values)

#depth
#values = {"pair": "nyc_usd"}
#serviceResp = yob.queryV3('depth', values)

#trades
#values = {"pair": "nyc_usd"}
#serviceResp = yob.queryV3('trades', values)

#---------------------------------------------------------

#API privada

#getinfo
#values = {}
#serviceResp = yob.queryV3('getInfo', values)

#Trade
#values = {"pair": "nyc_usd", "type": "buy", "rate": "112", "amount": "100"}
#serviceResp = yob.queryV3('Trade', values)

#ActiveOrders
#values = {"pair": "nyc_usd"}
#serviceResp = yob.queryV3('ActiveOrders', values)

#OrderIfo
#values = {"order_id": "206167381996163"}
#serviceResp = yob.queryV3('OrderInfo', values)

#CancelOrder
#values = {"order_id": "206167381111163"}
#serviceResp = yob.queryV3('CancelOrder', values)

#GetDepositAddress
#values = {"coinName": "NYC", "need_new": 0}
#serviceResp = yob.queryV3('GetDepositAddress', values)

#WithdrawCoinsToAddress
#values = {"coinName": "NYC", "amount": '100000', 'address': 'RQRT7MyVFM3PxAXnt5FkBCiruPyaQLzcGM'}
#serviceResp = yob.queryV3('WithdrawCoinsToAddress', values)

print(serviceResp)

